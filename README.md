# gimmeip

To install this, use 

`make && make install`

if all goes well it will be installed to /usr/local/bin/gimmeip

## Usage

Usage is simple, just run the binary and give it a hostname

`gimmeip google.com`

