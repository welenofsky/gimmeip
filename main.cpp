#include <iostream>
#include <netdb.h>
#include <arpa/inet.h>

int main(int argc, const char * argv[]) {
    if (argc < 2) {
        printf("Usage: %s hostname\n", argv[0]);
        return 1;
    }
    
    struct hostent *hp = gethostbyname(argv[1]);
    
    if (hp == NULL) {
        std::cout << "Unable to get host by name" << std::endl;
    } else {
        unsigned int i = 0;
        while ( hp->h_addr_list[i] != NULL) {
            std::cout << inet_ntoa( *( struct in_addr*)(hp->h_addr_list[i])) << std::endl;
            i++;
        }
    }
    
    return 0;
}
