CXX = g++
CXXFLAGS = -Wall -g

main: main.cpp 
	$(CXX) $(CXXFLAGS) main.cpp -o gimmeip

install: gimmeip
	install -d /usr/local/bin/
	install gimmeip /usr/local/bin/
